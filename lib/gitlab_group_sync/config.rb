require 'erb'
require 'pathname'
require 'slop'
require 'yaml'

module GitlabGroupSync
  class Config < Hash

    DEFAULT_OPTIONS = {
      config_dir:   'config',
      config_files: [],
      log_level:    :info,
      noop:         false,
    }.freeze

    MANDATORY_CONFIGS = (
      %i(host port method base bind_dn password).map{|e| [:ldap, :client, e]} +
      %i(endpoint private_token).map{|e| [:gitlab, :client, e]}
    ).freeze

    include GitlabGroupSync::Logger::Concern

    def initialize(args = [])
      @options = parse_args(args)
      GitlabGroupSync::Logger.configure(options)

      load_config_dir("#{__dir__}/../../default_config")
      load_config_dir(options[:config_dir])     if options[:config_dir]
      load_config_files(options[:config_files]) if options[:config_files]
      check_config
    end

    def check_config    
      missing = []

      MANDATORY_CONFIGS.each do |check|
        val = dig(*check.map(&:to_sym))
        if val.to_s.empty?
          log "Missing configuration value: #{check.join('/')}", :fatal
          missing << check
        end

      end
      
      abort unless missing.empty?
    end

    def load_config_dir(dir)
      dir = Pathname(dir)
      log "Loading configuration directory: #{dir.expand_path}", :debug
      load_config_files(Pathname(dir).children)
    end

    def load_config_file(file)
      file = Pathname(file)
      if %w(.yml .yaml).include?(file.extname)
        log "Loading configuration file: #{file.expand_path}", :debug
        deep_merge!(YAML.load(ERB.new(File.read(file)).result))
      else
        log "Invalid YAML configuration file extension, skipping: " +
          "#{file.expand_path}"; :warn
      end
    end

    def load_config_files(files)
      files.each{|file| load_config_file(file)}
    end

    def noop?
      !!options[:noop]
    end
    
    def options
      @options ||= {}
    end

    def parse_args(args)
      opts = Slop.parse(args) do |o|

        default_config_dir = DEFAULT_OPTIONS[:config_dir]
        o.string '-d', '--config-dir',
          'the directory containing YAML configuration files',
          default: (default_config_dir if Dir.exists?(default_config_dir))

        o.array '-f', '--config-file',
          'a list of YAML configuration files to read',
          default: DEFAULT_OPTIONS[:config_files]

        o.string '-l', '-log-file',
          'the file to send logs to'

        o.string '-error-log-file',
          'the file to send error logs to'

        o.string '--log-level',
          'the minimum log level to display',
          default: DEFAULT_OPTIONS[:log_level]

        o.bool '-n', '--noop',
          'skip any change that should be made',
          default: DEFAULT_OPTIONS[:noop]

        o.on '-V', '--version', 'print the version' do
          puts "#{self} version #{self::VERSION}"
          exit
        end

        o.on '-h', '--help', 'give this help list' do
          puts o
          exit
        end
      end

      opts.to_h
    end
  end
end