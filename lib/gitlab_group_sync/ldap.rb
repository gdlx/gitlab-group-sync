require "active_ldap"
require "forwardable"

module GitlabGroupSync
  module LDAP
    module Concern
      extend Forwardable
      def_delegator GitlabGroupSync::LDAP, :find, :ldap_find
    end

    class << self
      include GitlabGroupSync::Logger::Concern

      def configure(config)
        connect(config[:client])
      end

      def connect(client)
        host = "#{client[:host]}:#{client[:port]}"

        ActiveLdap::Base.setup_connection(client)
        ActiveLdap::Base.find('test')
      rescue ActiveLdap::EntryNotFound
        log "ActiveLdap successfully connected to '#{host}'", :debug
      rescue ActiveLdap::AuthenticationError
        log "ActiveLdap failed to connect to '#{host}'", :fatal
        abort
      end

      def find(*attrs)
        log "ActiveLdap find query: #{attrs}", :debug
        result = ActiveLdap::Base.find(*attrs)
        log "LDAP object not found: #{attrs}", :warn unless result
        return result
      rescue ActiveLdap::EntryNotFound => e
        log e.to_s, :warn
        return nil
      end
    end
  end
end
