require "forwardable"
require "logger"

module GitlabGroupSync
  module Logger
    module Concern
      extend Forwardable
      def_delegators GitlabGroupSync::Logger, :log
    end
    
    def self.configure(o = {})
      std_log_dev = o[:log_file] || STDOUT
      @@logger_std = ::Logger.new(std_log_dev, level: o[:log_level])

      err_log_dev = o[:error_log_file] || o[:log_file] || STDERR
      @@logger_err = if err_log_dev == std_log_dev
        @logger_std
      else
        ::Logger.new(err_log_dev, level: o[:log_level])
      end
    end

    def self.log(message, level = nil)
      level ||= :info
      logger(level).send(level, message)
    end

    def self.logger(level)
      %i(warn error fatal).include?(level) ? @@logger_err : @@logger_std
    end
  end
end