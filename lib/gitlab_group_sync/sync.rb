module GitlabGroupSync
  class Sync
    include GitlabGroupSync::Gitlab::Concern
    include GitlabGroupSync::LDAP::Concern
    include GitlabGroupSync::Logger::Concern

    attr_reader(
      :config,
      :content_group_configs,
      :gitlab_group_paths_by_id,
      :gitlab_groups,
      :gitlab_local_users,
      :gitlab_users,
      :ldap_groups,
      :ldap_users,
      :ldap_users_by_cn,
      :security_group_configs,
    )

    alias_method :c, :config

    # Initializes synchronization task
    #
    # @param [ Hash ] config The configuration hash
    # @param [ Hash ] options The synchronization options
    #
    # @option options [ true, false ] :noop Whether sync shoud skip changes
    def initialize(config, options = {})
      @config  = config
      @options = options

      apply_default_config
    end

    # Executes synchronization tasks
    def execute
      log "=== Starting Gitlab group synchronization ==="
      log "NOOP enabled, skipping changes", :warn if noop?

      reset_global_variables
      fetch_gitlab_groups
      fetch_gitlab_users
      sync_security_groups(config[:security_groups])
      update_users_attributes
      sync_content_groups(config[:content_groups])

      log "=== Gitlab group synchronization has ended successfully ==="
    end

  private

    # Defines default configuration values
    def apply_default_config
      config[:gitlab][:admin_user]    ||= 'root'
      config[:gitlab][:max_page_size] ||= 100
    end

    # Fetches all groups from Gitlab to cache them and avoid many unitary calls
    def fetch_gitlab_groups
      log "Fetching Gitlab groups"
      per_page = config[:gitlab][:max_page_size]
      i = 1; loop do
        groups = gitlab.groups(page: i, per_page: per_page)
        groups.each do |group|
          gitlab_groups[group.full_path]     = group
          gitlab_group_paths_by_id[group.id] = group.full_path
        end
        break if groups.count < per_page
        i += 1
      end
      log "Found #{gitlab_groups.count} groups", :debug
    end

    # Fetches all users from Gitlab to cache them and avoid many unitary calls
    def fetch_gitlab_users
      log "Fetching Gitlab groups"
      per_page = config[:gitlab][:max_page_size]

      i = 1; loop do
        users = gitlab.users(page: i, per_page: per_page)
        users.each do |user|
          username = user.username.downcase
          gitlab_users[username] = user
        end
        break if users.count < per_page
        i += 1
      end
      log "Found #{gitlab_users.count} users", :debug
    end

    # Fetches a group members from Gitlab and maps them with LDAP users.
    #
    # @param [ Gitlab::ObjectifiedHash ] group The gitlab group
    #
    # @return [ Hash ] Usernames as keys, access levels as value
    def fetch_gitlab_group_members(group)
      log "Fetching Gitlab group '#{group.full_path}' members", :debug
      per_page = config[:gitlab][:max_page_size]
      gitlab_members = {}

      i = 1; loop do
        members = gitlab.group_members(group.id, page: i, per_page: per_page)
        members.each do |member|
          username = member.username.downcase
          user     = gitlab_users[username]
          is_ldap  = ldap_users.key?(username)

          unless is_ldap || gitlab_local_users.key?(username)
            is_ldap = if (
              !user.identities.empty? &&
              ldap_user = ldap_find(:first, filter: {cn: username})
            )
              true
              #ldap_users[username] = ldap_user
            else
              gitlab_local_users[username] = user
              false
            end
          end

          if user && is_ldap
            gitlab_members[username] = gitlab_access_name(member.access_level)
          end
        end
        break if members.count < per_page
        i += 1
      end

      log "Found #{gitlab_members.count} members in Gitlab group '#{group.full_path}'", :debug

      return gitlab_members
    end

    # Looks for a previously cached Gitlab group and create it if necessary.
    #
    # @param [ Hash ] group The group options
    # @param [ Hash ] parent The parent group options, if any
    #
    # @return [ Gitlab::ObjectifiedHash ] The Gitlab group
    def find_or_create_gitlab_group(group, parent = nil)
      full_path = group[:full_path]
      gitlab_group = gitlab_groups[full_path]
      group[:name]       ||= group[:path].titleize
      group[:visibility] ||= 'private'

      unless gitlab_group
        msg = "Group '#{full_path}' does not exists on Gitlab, creating"
        return if log_noop? msg, :warn

        group_opts = {}
        group_opts[:parent_id]  = parent[:id] if parent
        group_opts[:visibility] = group[:visibility]
        gitlab_group = gitlab.create_group(
          group[:name], group[:path], group_opts)
        gitlab_groups[full_path] = gitlab_group
      end

      update_group_attributes(group)

      return gitlab_group
    end

    # Provides the Gitlab admin (root) user
    #
    # @return [ Gitlab::ObjectifiedHash ] The Gitlab admin user
    def gitlab_admin
      return @gitlab_admin if @gitlab_admin

      @gitlab_admin = gitlab.user_search(config[:gitlab][:admin_user]).first
    end

    # Logs an action and adds a prefix indicating that the action will be
    # skipped if synchronization is executed in NOOP mode.
    #
    # @param [ String ] msg The action log message
    # @param [ Symbol ] level The action log level
    #
    # @return [ true, false ] Whether synchronization is executed in NOOP mode
    def log_noop?(msg, level = nil)
      msg = "[SKIPPED] #{msg}" if noop?
      log msg, level
      noop?
    end

    # Whether synchronization is executed in NOOP mode.
    #
    # @return [ true, false ] Whether synchronization is executed in NOOP mode
    def noop?
      !!@options[:noop]
    end

    # Initializes global variables
    def reset_global_variables
      @content_group_configs = {}
      @gitlab_group_paths_by_id = {}
      @gitlab_groups = {}
      @gitlab_local_users = {}
      @gitlab_users = {}
      @ldap_groups = {}
      @ldap_users = {}
      @ldap_users_by_cn = {}
      @security_group_configs = {}
    end

    # Analyses a content groups sharing definitions and provides the expected
    # Gitlab group sharings result.
    #
    # @param [ Array<Hash> ] sharings Sharing definitions
    # @param [ Array<Hash> ] parent_sharings Parent sharing definitions
    #
    # @return [ Hash ] Expected Gitlab group sharings
    def resolve_group_sharing_patterns(sharings, parent_sharings = {})
      return parent_sharings unless sharings.is_a?(Array)

      resolved_sharings = parent_sharings.clone

      sharings.each do |sharing|
        max_access_level = gitlab_access_level(sharing[:max_access_level])

        patterns = sharing.fetch(:patterns, [])
        patterns << /#{sharing[:pattern]}/ if sharing[:pattern]
        paths    = sharing.fetch(:paths,    [])
        paths    << sharing[:path] if sharing[:path]
        patterns += paths.map{|p| /^#{p}$/}

        groups = security_group_configs.keys.select do |path|
          patterns.any? do |pattern|
            pattern.match(path)
          end
        end

        groups.each do |path|
          case max_access_level
          when 0   then resolved_sharings.delete(path)
          when nil then false
          else resolved_sharings[path] = sharing[:max_access_level]
          end
        end
      end

      return resolved_sharings
    end

    # Analyses a security group LDAP mappings and provides the mapped Gitlab
    # group members.
    #
    # @param [ Hash ] The security group definition
    #
    # @return [ Array ] The mapped Gitilab group members
    def resolve_ldap_group_members(options)
      skip = false
      members = []
      ldap_base = options[:ldap_base]

      options.fetch(:ldap_members, []).each do |cn|
        user = ldap_users.fetch(cn,
          ldap_find(:first, filter: {cn: cn}, base: ldap_base)
        )
        if user
          members << user
        else
          skip = true
        end
      end

      groups = options.fetch(:ldap_groups, [])
      groups << options[:ldap_group] if options[:ldap_group]
      groups.each do |group|
        action = /^-/.match(group) ? :sub : :add
        cn  = /^-?(.*)$/.match(group)[1]
        group = ldap_groups.fetch(cn,
          ldap_find(:first, filter: {cn: cn}, base: ldap_base)
        )
        unless group
          skip = true
          next
        end

        attribute = options[:ldap_member_attribute]
        users = group.send(attribute).reject{|m| m.empty?}
        users = users.map{|u| ldap_find(u)}

        case action
        when :add then members += users
        when :sub then members -= users
        end
      end

      return if skip

      resolved_members = []
      members.each do |member|
        cn = member.cn.downcase
        ldap_users[cn] = member
        resolved_members << cn if @gitlab_users[cn]
      end

      return resolved_members.compact.uniq
    end

    # Applies content groups definitions to Gitlab.
    # This method is recursive and calls itself to manage subgroups.
    #
    # @param [ Array ] groups Content group definitions
    # @param [ Hash ] options Content groups sync options
    #
    # @option options [ String ] :parent_path The parent group path
    def sync_content_groups(groups, options = {})
      log "Synchronizing content groups" if options.empty?

      parent = content_group_configs.fetch(options[:parent_path], {})
      path_parts      = parent.fetch(:path_parts, [])
      parent_sharings = parent.fetch(:sharings, {})

      groups.each do |group|

        if group[:paths].is_a?(Array)
          subgroups = group.delete(:paths).map do |path|
            group.merge(path: path)
          end
          sync_content_groups(subgroups, options)
          next
        end

        group[:path_parts] = path_parts + [group[:path]]
        group[:full_path]  = group[:path_parts].join('/')
        full_path = group[:full_path]

        log "Synchronizing content group '#{full_path}'", :debug

        # Inherited attributes
        copy_attrs = %i(visibility)
        group = parent.slice(*copy_attrs).merge(group)

        next unless gitlab_group = find_or_create_gitlab_group(group, parent)

        group_id    = group[:id] = gitlab_group.id
        members     = gitlab.group_members(group_id)
        owner_level = gitlab_access_level(:owner)
        admin_name  = gitlab_admin.username
        admin_id    = gitlab_admin.id

        # Group members synchronization
        if members.none?{|m| m.id == admin_id}
          unless log_noop? "Adding user '#{admin_name}' as 'owner' " +
            "on group '#{full_path}'", :warn
            gitlab.add_group_member(group_id, admin_id, owner_level)
          end
        elsif members.any?{|m| m.id == admin_id && m.access_level != owner_level}
          unless log_noop? "Updating '#{admin_name}' access level to 'owner'" +
            "on group '#{full_path}'", :warn
            gitlab.edit_group_member(group_id, admin_id, owner_level)
          end
        end
        members.each do |member|
          next if member.id == admin_id
          next unless ldap_users[member.username]
          unless log_noop? "Removing member '#{member.name}' " +
            "from group '#{full_path}'", :warn
            gitlab.remove_group_member(group_id, member.id)
          end
        end

        target_sharings = group[:sharings] =
          resolve_group_sharing_patterns(group[:sharings], parent_sharings)

        content_group_configs[full_path] = group

        # Projects access synchronization
        gitlab.group_projects(group_id).each do |project|
          project_path = project.path_with_namespace
          log "Synchronizing project '#{project_path}'", :debug

          # Project members synchronization (no LDAP user allowed)
          members = gitlab.team_members(project.id)
          members.each do |member|
            next unless ldap_users[member.username]
            unless log_noop? "Removing member '#{member.username}' " +
              "from project #{project_path}", :warn
              gitlab.remove_team_member(project.id, member.id)
            end
          end

          # Project group sharings synchronization
          current_sharings = {}
          project.shared_with_groups.each do |s|
            path = gitlab_group_paths_by_id[s['group_id']]
            current_sharings[path] = gitlab_access_name(s['group_access_level'])
          end

          next if current_sharings == target_sharings

          unless target_sharings.empty? && current_sharings.empty?
            log "Current '#{project_path}' sharings: #{current_sharings}", :debug
            log "Target  '#{project_path}' sharings: #{target_sharings}",  :debug
          end

          group_paths = current_sharings.keys + target_sharings.keys
          group_paths.uniq.sort.each do |path|
            g = gitlab_groups[path]
            access_name  = target_sharings[path]
            access_level = gitlab_access_level(access_name)

            if target_sharings[path] && !current_sharings[path]
              unless log_noop? "Sharing project '#{project_path}' " +
                "with group '#{g.full_path}' as '#{access_name}'", :warn
                gitlab.share_project_with_group(project.id, g.id, access_level)
              end
            elsif current_sharings[path] && !target_sharings[path]
              unless log_noop? "Removing project '#{project_path}' sharing " +
                "with group '#{g.full_path}'", :warn
                gitlab.delete("/projects/#{project.id}/share/#{g.id}")
              end
            elsif target_sharings[path] != current_sharings[path]
              unless log_noop? "Updating project '#{project_path}' sharing " +
                "with group '#{g.full_path}' " +
                "to access level '#{target_sharings[path]}'", :warn
                gitlab.delete("/projects/#{project.id}/share/#{g.id}")
                gitlab.share_project_with_group(project.id, g.id, access_level)
              end
            end
          end
        end

        # Subgroups discovery
        discover_level = group[:discover_subgroups_level]
        level = discover_level == :all ? 1 : discover_level.to_i
        if level > 0
          subgroups = gitlab.group_subgroups(group_id)
          if subgroups.count > 0
            log "Discovered #{subgroups.count} subgroups " +
              "in group '#{full_path}'", :debug
            group[:subgroups] = subgroups.map do |g|
              level = discover_level == :all ? :all : (level - 1)
              {path: g.path, name: g.name, discover_subgroups_level: level}
            end
          end
        end

        # Subgroups recursion
        if group[:subgroups].is_a?(Array)
          sync_content_groups(group[:subgroups], parent_path: full_path)
        end
      end

      if options.empty?
        log "Synchronized #{content_group_configs.count} content groups"
      end
    end

    # Applies security groups definitions to Gitlab.
    # This method is recursive and calls itself to manage subgroups.
    #
    # @param [ Array ] groups Security group definitions
    # @param [ Hash ] options Security groups sync options
    #
    # @option options [ String ] :parent_path The parent group path
    def sync_security_groups(groups, options = {})
      log "Synchronizing security groups" if options.empty?

      parent = security_group_configs.fetch(options[:parent_path], {})
      path_parts = parent.fetch(:path_parts, [])

      groups.each do |group|
        group[:path_parts] = path_parts + [group[:path]]
        group[:full_path]  = group[:path_parts].join('/')
        full_path = group[:full_path]

        log "Synchronizing security group '#{full_path}'", :debug

        # Inherited attributes
        copy_attrs = %i(access_level ldap_base ldap_member_attribute visibility)
        group = parent.slice(*copy_attrs).merge(group)

        next unless gitlab_group = find_or_create_gitlab_group(group, parent)

        group[:id] = gitlab_group.id
        security_group_configs[full_path] = group

        target_members = resolve_ldap_group_members(group)
        unless target_members
          log "Some security group '#{full_path}' LDAP members have not been " +
            "matched, skipping to avoid security breach", :warn
          next
        end

        target_members  = Hash[target_members.map{|m| [m, group[:access_level]]}]
        current_members = fetch_gitlab_group_members(gitlab_group)

        if target_members.count > 0 || current_members.count > 0
          log "Current '#{full_path}' members: #{current_members}", :debug
          log "Target  '#{full_path}' members: #{target_members}",  :debug
        end

        (current_members.keys + target_members.keys).uniq.sort.each do |username|
          user         = gitlab_users[username]
          access_name  = target_members[username]
          access_level = gitlab_access_level(access_name)

          if target_members[username] && !current_members[username]
            unless log_noop? "Adding member '#{username}' to group " +
              "'#{full_path}' with access level '#{access_name}'", :warn
              gitlab.add_group_member(gitlab_group.id, user.id, access_level)
            end
          elsif current_members[username] && !target_members[username]
            unless log_noop? "Removing member '#{username}' from group " +
              "'#{full_path}'", :warn
              gitlab.remove_group_member(gitlab_group.id, user.id)
            end
          elsif target_members[username] != current_members[username]
            unless log_noop? "Updating member '#{username}' access level " +
              "in group '#{full_path}' from '#{current_members[username]}' " +
              "to '#{access_name}'", :warn
              gitlab.edit_group_member(gitlab_group.id, user.id, access_level)
            end
          end
        end

        # Subgroups recursion
        if group[:subgroups].is_a?(Array)
          sync_security_groups(group[:subgroups], parent_path: full_path)
        end
      end

      if options.empty?
        log "Synchronized #{security_group_configs.count} security groups"
      end
    end

    # Update Gitlab group attributes.
    #
    # @param [ Hash ] group The Gitlab group definition
    def update_group_attributes(group)
      full_path    = group[:full_path]
      gitlab_group = gitlab_groups[full_path]
      update_attrs = {}
      group_attrs  = {
        name:        group[:name],
        description: group[:description],
        visibility:  group[:visibility],
      }

      group_attrs.each do |k,v|
        next if v.nil?

        update_attrs[k] = v unless gitlab_group.send(k) == v
      end

      unless update_attrs.empty?
        msg = "Updating group '#{full_path}' attributes: #{update_attrs}"

        # Visibility key is added because of https://gitlab.com/gitlab-org/gitlab-ce/issues/48764
        update_attrs[:visibility] ||= gitlab_group.visibility

        unless log_noop? msg, :warn
          gitlab.edit_group(gitlab_group.id, update_attrs)
        end
      end
    end

    # Updates Gitlab users attributes.
    def update_users_attributes
      log "Updating user attributes"

      internal_user_ldap_bases = config[:ldap][:internal_user_bases]
      internal_user_ldap_bases = internal_user_ldap_bases.map{|b| /#{b}$/}

      gitlab_users.each do |username,user|
        ldap_user = ldap_users[username]

        update_attrs = {}
        if ldap_user
          log "Updating affected LDAP user '#{username}'", :debug
          full_name = %i(givenName surname)
          full_name = full_name.map{|e| ldap_user.send(e).strip}.join(' ')
          update_attrs[:name] = full_name if user.name != full_name

          external = internal_user_ldap_bases.none?{|b| b.match(ldap_user.dn)}
          update_attrs[:external] = external if user.external != external

          user_type = external ? :external : :internal
        elsif !user.identities.empty?
          log "Updating unaffected LDAP user '#{username}'", :debug
          user_type = :unaffected
        else
          log "Skipping local user '#{username}'", :debug
          next
        end

        user_config = config[:gitlab][:user_configs][user_type]

        user_config.each do |key, value|
          update_attrs[key] = value unless user.send(key) == value
        end

        if user_config[:state] && user_config[:state] != user.state
          action, action_msg = case user_config[:state].to_sym
            when :active  then [:unblock_user, 'Unblocking']
            when :blocked then [:block_user,   'Blocking']
            else nil
          end

          if action
            unless log_noop? "#{action_msg} #{user_type} user '#{username}'", :warn
              gitlab.send(action, user.id)
            end
            update_attrs.delete(:state)
          end
        end

        unless update_attrs.empty?
          unless log_noop? "Updating user '#{username}' attributes: #{update_attrs}", :warn
            gitlab.edit_user(user.id, update_attrs)
          end
        end
      end
    end
  end
end
