require "forwardable"
require "gitlab"

module GitlabGroupSync
  module Gitlab
    module Concern
      extend Forwardable
      def_delegator GitlabGroupSync::Gitlab, :client,       :gitlab
      def_delegator GitlabGroupSync::Gitlab, :access_level, :gitlab_access_level
      def_delegator GitlabGroupSync::Gitlab, :access_name,  :gitlab_access_name
    end

    ACCESS_LEVELS = {
      none:        0,
      guest:      10,
      reporter:   20,
      developer:  30,
      maintainer: 40,
      owner:      50,
    }

    class << self
      include GitlabGroupSync::Logger::Concern

      def access_level(name)
        ACCESS_LEVELS[name]
      end

      def access_name(level)
        ACCESS_LEVELS.invert[level]
      end

      def configure(config)
        connect(config[:client])
      end

      def connect(config)
        endpoint = config[:endpoint]
        @@client = ::Gitlab.client(config)
        client.user
        log "Gitlab successfully connected to '#{endpoint}'", :debug
      rescue ::Gitlab::Error::MissingCredentials,
             ::Gitlab::Error::Unauthorized => e
        log "Gitlab authentication error: #{e}", :fatal
        abort
      rescue ::Errno::ECONNREFUSED => e
        log "Gitlab connection error: #{e}", :fatal
        abort
      end

      def client
        return @@client
      end
    end
  end
end
