require "active_support"
require "forwardable"
require "gitlab_group_sync/version"
require "gitlab_group_sync/logger"
require "gitlab_group_sync/config"
require "gitlab_group_sync/ldap"
require "gitlab_group_sync/gitlab"
require "gitlab_group_sync/sync"

module GitlabGroupSync
  def self.run(args)
    config = GitlabGroupSync::Config.new(args)
    
    GitlabGroupSync::LDAP.configure(config[:ldap])
    GitlabGroupSync::Gitlab.configure(config[:gitlab])

    sync = GitlabGroupSync::Sync.new(config.to_h, config.options)
    sync.execute
  end
end
