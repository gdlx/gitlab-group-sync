FROM ruby:alpine AS build-env
COPY . /gem
RUN \
  apk --no-cache add git && \
  cd gem && \
  rake build

FROM alpine
COPY --from=build-env /gem/pkg /pkg
RUN \
  apk update && \
  apk --no-cache add \
    ca-certificates \
    libldap \
    ruby \
    ruby-bigdecimal \
    ruby-json \
    && \
  gem install --no-document pkg/*.gem && \
  rm -rf /pkg /var/cache/apk/*
ENTRYPOINT ["gitlab-group-sync"]
