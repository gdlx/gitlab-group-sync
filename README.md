# Gitlab Group Sync

## Description

This gem manages security groups synchronization between Gitlab and a LDAP
server, content groups hierarchy, and security rules between security and
content groups.

## Requirements

  * Ruby >= 2.2
  * Gitlab >= 11.0

## Usage

The gem can be locally installed with `rake install` or built into a container
image with :

```bash
docker build -t gitlab_group_sync .
```

You can mount a local configuration directory to the container in `/config`. All
`.yml` and `.yaml` files in this directory will be loaded:

```bash
docker run -v $(pwd)/config:/config \
  -e "GITLAB_API_ENDPOINT=$GITLAB_API_ENDPOINT" \
  -e "GITLAB_API_PRIVATE_TOKEN=$GITLAB_API_PRIVATE_TOKEN" \
  -e "LDAP_BASE=$LDAP_BASE" \
  -e "LDAP_BIND_DN=$LDAP_BIND_DN" \
  -e "LDAP_HOST=$LDAP_HOST" \
  -e "LDAP_PASSWORD=$LDAP_PASSWORD" \
  gitlab_group_sync
```

The following CLI options are available:

```
usage: /usr/bin/gitlab_group_sync [options]
    -d, --config-dir   the directory containing YAML configuration files
    -f, --config-file  a list of YAML configuration files to read
    -l, -log-file      the file to send logs to
    -error-log-file    the file to send error logs to
    --log-level        the minimum log level to display
    -n, --noop         skip any change that should be made
    -V, --version      print the version
    -h, --help         give this help list
```

## Configuration

The gem is packaged with default configuration files allowing to define LDAP
and Gitlab connection information through the following environment variables:

  * `GITLAB_API_ENDPOINT`: Gitlab API endpoint
    (should be `https://<GITLAB HOSTNAME>/api/v4`)
  * `GITLAB_API_PRIVATE_TOKEN`: Gitlab user private token
    (must be an administrator)
  * `LDAP_BASE`: LDAP search base (optional)
  * `LDAP_BIND_DN`: LDAP user DN (read only is sufficient)
  * `LDAP_HOST`: LDAP server address
  * `LDAP_METHOD`: LDAP connection method
    (available: `ssl`, `tls`, `plain` ; default: `ssl`)
  * `LDAP_PASSWORD`: LDAP user DN password
  * `LDAP_PORT`: LDAP server port (default: `636`)

These configuration elements can also be provided in YAML configuration files
with the following format:

```yaml
:gitlab:
  :client:
    :endpoint: GITLAB_API_ENDPOINT
    :private_token: GITLAB_API_PRIVATE_TOKEN
:ldap:
  :client:
    :base: LDAP_BASE
    :bind_dn: LDAP_BIND_DN
    :host: LDAP_HOST
    :method: LDAP_METHOD
    :password: LDAP_PASSWORD
    :port: LDAP_PORT
```

All other configurations elements must be provided through YAML files:

### LDAP

```yaml
---
:ldap:
  # Array of LDAP DN bases whose users must be considered as internal
  :internal_user_bases:
    - ou=Internal,ou=People,dc=mycompany,dc=com
```

### Gitlab

```yaml
---
:gitlab:
  # Administrator username ; will be set as owner of all managed groups
  :admin_user: root 

  # Gitlab API is paginated by default. This defines the max elements per page
  :max_page_size: 100

  # User configurations
  # Configure :ldap/:internal_user_bases to select internal users
  :user_configs:
    :internal:
      # How many personal projects a user can have
      :projects_limit: 100
      # Whether a user can create a group
      :can_create_group: false
      # Internal users are unlocked
      :state: active
    :external:
      # External users can't create personal projects
      :projects_limit: 0
      :can_create_group: false
```

### Security groups

```yaml
---
:security_groups:
  # Root security group
- :path: security
  :description: Security groups that can be authorized on content groups.
  :access_level: :maintainer
  :ldap_member_attribute: uniqueMember
  :subgroups:
    # Internal groups root
  - :path: internal
    :name: Internal services
    # LDAP base will be inherited by subgroups
    :ldap_base: ou=Internal,ou=Groups,dc=mycompany,dc=com
    :subgroups:
    - :path: tech
      :name: Technical Services
      :ldap_group: TECH
    - :path: support
      :name: Support Services
      # Multiple LDAP groups can be mapped
      :ldap_groups: [SUPPORT, TECH]
    - :path: dev
      :name: Development Services
      # Some LDAP groups can also be substracted
      :ldap_groups: [DEV, -SUPPORT]

  - :path: mycustomer
    :name: My Customer
    :ldap_base: ou=Customers,ou=People,dc=mycompany,dc=com
    # Group members can be statically mapped by username
    :ldap_members: [john.doe, jane.doe]
```

### Content groups

```yaml
---
:content_groups:

  - :path: internal
    :name: Internal contents # The default name is the capitalized path
    # Visivbility can be set to private, internal or public (private by default)
    :visibility: internal
    :description: Internal projects
    :sharings:
        # Sharings are defined by security group path. It can be a regex:
      - :pattern: ^security/internal/
        :max_access_level: :reporter
        # Or a static path:
      - :path: security/internal/tech
        :max_access_level: :maintainer
    # Subgroups inherit parent's sharings
    :subgroups:
        # Multiple paths can be provided to quickly create multiple groups:
      - :paths: [artifacts, platform]
        # They can share specific sharings:
      - :paths: [products, scripts]
        :sharings:
            # Subgroups sharing can deny parent groups access:
          - :path: security/internal/tech
            :max_access_level: :reporter
          - :path: security/internal/dev
            :max_access_level: :maintainer
        # Nested subgroups can be discovered to apply access rules.
        # The provided integer defines maximum recursion level.
        # :all provides infinite recursion.
        :discover_subgroups_level: 1
```